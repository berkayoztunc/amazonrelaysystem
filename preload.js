// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.



const {
  ipcRenderer
} = require('electron');

// Some data that will be sent to the main process
// Feel free to modify the object as you wish !




window.addEventListener('DOMContentLoaded', () => {
  let a  = document.getElementsByName("x-csrf-token")[0] ? document.getElementsByName("x-csrf-token")[0].getAttribute("content") : null;
  let Data = {
    message: a
  };
  ipcRenderer.send('request-update-label-in-second-window', Data);
  setTimeout(()=>{
    location.reload();
  },1000*60*3)
})