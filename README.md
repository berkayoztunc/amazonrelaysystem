## Sistem kullanım aşaması

- amazon login
- AmazonRelay ekranından -> yenile
- Anlaşılır bir arama adı bir
- filtreden arama filtresi seç 
- arama saniye aralığı girin (en az 1 sn “şimdilik geliştirme olduğu için” sonradan o milisaniye cinsinde olucak)
- aramanın hangi saatten büyük olduğunu girin (mutlaka hh:mm şeklinde olmalıdır, örneği 19:23, 03:33 ,01:12 ,24:00)
- durak ekleyin
- işlem ekle tuşuna basın
## Durak ekleme tarifi
### tanım :
`start` : çıkış noktası (* yıldız işareti verilirse tüm hepsini kabul eder) 
`stop` : varış noktası (* yıldız işareti verilirse tüm hepsini kabul eder)
Sırası amazondaki sırasıyla birebir olmalıdır örn   
AVP3 -> BUF5  BUF5 -> AVP3